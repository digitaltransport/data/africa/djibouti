# Djibouti
## Djibouti Bus route mapping project
Following an official request from the Djibouti government, the World Bank proposed to provide technical assistance on urban transport to the Ministry of Equipment and Transport (MET).

Fieldwork was done between May and October of 2019.

### Available Files:

#### Surveys:

Raw data from passenger, driver and off-bus surveys

#### Existing routes: 
Existing bus routes in shapefile format.

**Metadata:**

**name:** Serial number given to each route

**AM_FREQ:** Frequency of buses during daytime

**Origin:** Route's origin terminal

**Dest:** Route's destination terminal


### Credits:


*  Mobility and Logistics (MOLO) Multi-donor Trust Fund
*  World Bank Group
*  In partnership with the University of Djibouti and the Ministry of Equipment and Transport of Djibouti